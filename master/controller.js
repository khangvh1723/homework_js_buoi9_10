
function domForm() {
    // dom thông tin nv từ form
    // input
    tknv = document.getElementById("tknv").value;
    name = document.getElementById("name").value;
    email = document.getElementById("email").value;
    password = document.getElementById("password").value;
    datepicker = document.getElementById("datepicker").value;
    luongCB = document.getElementById("luongCB").value*1;
    chucvu = document.getElementById("chucvu").value;
    gioLam = document.getElementById("gioLam").value*1;
    // output
    console.log(tknv);
    return new NhanVien(tknv, name, email, password, datepicker, luongCB, chucvu, gioLam);
}

function showForm(nv) {
    document.getElementById("tknv").value = nv.tknv;
    document.getElementById("tknv").readOnly = true;
    document.getElementById("name").value = nv.name;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.password;
    document.getElementById("datepicker").value = nv.datepicker;
    document.getElementById("luongCB").value = nv.luongCB;
    document.getElementById("chucvu").value = nv.chucvu;
    document.getElementById("gioLam").value = nv.gioLam;
}

function resetForm() {
    document.getElementById("tknv").value = "";
    document.getElementById("tknv").readOnly = false;
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
    document.getElementById("datepicker").value = "";
    document.getElementById("luongCB").value = "";
    document.getElementById("chucvu").value = "";
    document.getElementById("gioLam").value = "";
}

function renderDSNV(dsnv) {
    // input: dsnv array
    // process
    var contentHTML = "";
    for (var i = 0; i < dsnv.length; i++) {
        var nv = dsnv[i];
        var content = `
        <tr>
        <td>${nv.tknv}</td>
        <td>${nv.name}</td>
        <td>${nv.email}</td>
        <td>${nv.datepicker}</td>
        <td>${nv.chucvu}</td>
        <td>${nv.tinhLuong()}</td>
        <td>${nv.xepLoaiNV()}</td>
        <td class="d-flex">
        <button class="btn btn-danger" onclick="deleteNV('${nv.tknv}')"><i class="fa fa-trash" aria-hidden="true"></i></button>
        <button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="editNV('${nv.tknv}')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
        </td>
        </tr>
        `
        contentHTML += content;
    }
    // output
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function postData(dsnv) {
    //#region push dssv into localStorage
    var dataJSON = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJSON);
    //#endregion
}