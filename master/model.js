function NhanVien(_tknv, _name, _email,_password,_datepicker,_luongCB,_chucvu,_gioLam) {
    this.tknv = _tknv;
    this.name = _name;
    this.email = _email;
    this.password = _password;
    this.datepicker = _datepicker;
    this.luongCB = _luongCB;
    this.chucvu = _chucvu;
    this.gioLam = _gioLam;
    this.tinhLuong = function() {
        switch(this.chucvu) {
            case "Nhân viên":
                return this.luongCB;
            case "Trưởng phòng":
                return this.luongCB*2;
            case "Sếp":
                return this.luongCB*3;
            default:
                return 0;
        }
    }
    this.xepLoaiNV = function() {
        if(this.gioLam<160){
            return "trung bình";
        } else if(this.gioLam<176){
            return "khá";
        } else if(this.gioLam<192){
            return "giỏi";
        } else {
            return "xuất sắc";
        }
    }
}