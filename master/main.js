var dsnv = [];
//#region Get data from localStorage
var dataJSON = localStorage.getItem("DSNV");
if (dataJSON != null) {
    // map
    dsnv = JSON.parse(dataJSON).map(item => {
        return new NhanVien(item.tknv, item.name, item.email, item.password, item.datepicker, item.luongCB, item.chucvu, item.gioLam);
    })
    renderDSNV(dsnv);
}
//#endregion
document.getElementById("btnThemNV").onclick = createNewNV;
function createNewNV() {
    // dom thông tin nv từ form
    var nv = domForm();
    // validate nv 
    var isValid =
    // check length tknv
    /^[0-9]{4-6}$/.test();
    // push object into dsnv array
    dsnv.push(nv);
    resetForm();
    renderDSNV(dsnv);
    //#region push dssv into localStorage
    var dataJSON = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJSON);
    //#endregion
}

function deleteNV(tknv) {
    var index = dsnv.findIndex(item => {
        return item.tknv == tknv;
    });
    dsnv.splice(index,1);
    postData(dsnv);
    renderDSNV(dsnv);
}

function editNV(tknv) {
    var index = dsnv.findIndex(item => {
        return item.tknv == tknv;
    });
    var nv = dsnv[index];
    showForm(nv);
}

document.getElementById("btnCapNhat").onclick = updateSV;
function updateSV() {
    // input
    var sv = domForm();
    // process
    var index = dsnv.findIndex(item => {
        return item.tknv == sv.tknv;
    });
    dsnv[index] = sv;
    // output
    postData(dsnv);
    renderDSNV(dsnv);
}

// btnDong
document.getElementById("btnDong").onclick = resetForm();