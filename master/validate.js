function showMessage(idSpan,message) {
    document.getElementById(idSpan).innerHTML = message;
}
function checkLengthTKNV(tknv) {
    const re = /^[0-9]{4,6}$/;
    var isTKNV = re.test(tknv);
    if(isTKNV) {
        showMessage("tbTKNV","");
        return true;
    } else {
        showMessage("tbTKNV","độ dài tài khoản phải từ 4-6");
        return false;    
    }
}
function checkValidName(name) {
    const re = /^[a-zA-Z]$/;
    var isName = re.test(name);
    if(isName) {
        showMessage("tbName","");
        return true;
    } else {
        showMessage("tbName","tên chỉ chứa ký tự chữ");
        return false;
    }
}
function checkRedundantTKNV(tknv,dsnv) {
    var index = dsnv.findIndex(item => {
        return item.tknv == tknv;
    })
    if(index == -1){
        showMessage("tbTKNV","");
        return true;
    } else {
        showMessage("tbTKNV","tài khoản đã tồn tại");
        return false;
    }
}
function checkValidEmail(email) {
    const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = re.test(email);
    if(isEmail){
        showMessage("tbEmail", "");
        return true;
    } else {
        showMessage("tbEmail", "email không hợp lệ");
        return false;
    }
}
function checkPassword(password) {
    const re = /^(?=.*?[0-9])(?=.*?[A-Z])(?=.*?[#?!@$%^&*-]).{6,8}$/;
    var isPassword = re.test(password);
    if(password){
        showMessage("tbPassword","");
        return true;
    }
    else{
        showMessage("tbPassword"),"password không hợp lệ";
    }
}